---
title: "FHA loan processor"
output: html_document
date: "2023-03-29"
---

This will work better with a directory set at the beginning, and working out of it to load, filter and bind all of the files over the determined time period.

```{r setup, include=FALSE}
library(tidyverse)
library(readxl)

jan2023p_pre <- read_xlsx("~/Downloads/FHA_SFSnapshot Forward_Jan2023.xlsx", sheet = "Purchase Data January 2023")

jan2023p_pre1 <- jan2023p_pre %>% filter(`Property State` == "IL", `Property City` == "CHICAGO")

feb2023p_pre <- read_xlsx("~/Downloads/FHA_SFSnapshot_Forward_Feb2023T.xlsx", sheet = "Purchase Data February 2023")

feb2023p_pre1 <- jan2023p_pre %>% filter(`Property State` == "IL", `Property City` == "CHICAGO")

jan2022p_pre <- read_xlsx("~/Downloads/FHA_SFSnapshot_Jan2022.xlsx", sheet = "Purchase Data January 2022")

jan2022p_pre1 <- jan2022p_pre %>% filter(`Property State` == "IL", `Property City` == "CHICAGO")

colnames(jan2022p_pre1)[5] <- "Origination_Mortgagee_Sponsor_Or"
colnames(jan2022p_pre1)[6] <- "Orig_Num"
colnames(jan2022p_pre1)[7] <- "Sponsor_Name"
colnames(jan2022p_pre1)[8] <- "Sponsor_Num"
colnames(jan2022p_pre1)[9] <- "Down_Payment_Source"
colnames(jan2022p_pre1)[10] <- "Non_Profit_Number"
colnames(jan2022p_pre1)[11] <- "Product_Type"
colnames(jan2022p_pre1)[12] <- "Loan_Purpose"
colnames(jan2022p_pre1)[13] <- "Property_Type_Final"

colnames(jan2022p_pre1)[16] <- "Endorsement_Year"
colnames(jan2022p_pre1)[17] <- "Endorsement_Month"


feb2022p_pre <- read_xlsx("~/Downloads/FHA_SFSnapshot_Feb2022.xlsx", sheet = "Purchase Data February 2022")

feb2022p_pre1 <- jan2022p_pre %>% filter(`Property State` == "IL", `Property City` == "CHICAGO")

colnames(feb2022p_pre1)[5] <- "Origination_Mortgagee_Sponsor_Or"
colnames(feb2022p_pre1)[6] <- "Orig_Num"
colnames(feb2022p_pre1)[7] <- "Sponsor_Name"
colnames(feb2022p_pre1)[8] <- "Sponsor_Num"
colnames(feb2022p_pre1)[9] <- "Down_Payment_Source"
colnames(feb2022p_pre1)[10] <- "Non_Profit_Number"
colnames(feb2022p_pre1)[11] <- "Product_Type"
colnames(feb2022p_pre1)[12] <- "Loan_Purpose"
colnames(feb2022p_pre1)[13] <- "Property_Type_Final"

colnames(feb2022p_pre1)[16] <- "Endorsement_Year"
colnames(feb2022p_pre1)[17] <- "Endorsement_Month"


mar2022p_pre <- read_xlsx("~/Downloads/FHA_SFSnapshot_Mar2022.xlsx", sheet = "Purchase Data March 2022")

mar2022p_pre1 <- mar2022p_pre %>% filter(`Property State` == "IL", `Property City` == "CHICAGO")

colnames(mar2022p_pre1)[5] <- "Origination_Mortgagee_Sponsor_Or"
colnames(mar2022p_pre1)[6] <- "Orig_Num"
colnames(mar2022p_pre1)[7] <- "Sponsor_Name"
colnames(mar2022p_pre1)[8] <- "Sponsor_Num"
colnames(mar2022p_pre1)[9] <- "Down_Payment_Source"
colnames(mar2022p_pre1)[10] <- "Non_Profit_Number"
colnames(mar2022p_pre1)[11] <- "Product_Type"
colnames(mar2022p_pre1)[12] <- "Loan_Purpose"
colnames(mar2022p_pre1)[13] <- "Property_Type_Final"

colnames(mar2022p_pre1)[16] <- "Endorsement_Year"
colnames(mar2022p_pre1)[17] <- "Endorsement_Month"

apr2022p_pre <- read_xlsx("~/Downloads/FHA_SFSnapshot_Apr2022.xlsx", sheet = "Purchase Data April 2022")

apr2022p_pre1 <- apr2022p_pre %>% filter(`Property State` == "IL", `Property City` == "CHICAGO")

colnames(apr2022p_pre1)[5] <- "Origination_Mortgagee_Sponsor_Or"
colnames(apr2022p_pre1)[6] <- "Orig_Num"
colnames(apr2022p_pre1)[7] <- "Sponsor_Name"
colnames(apr2022p_pre1)[8] <- "Sponsor_Num"
colnames(apr2022p_pre1)[9] <- "Down_Payment_Source"
colnames(apr2022p_pre1)[10] <- "Non_Profit_Number"
colnames(apr2022p_pre1)[11] <- "Product_Type"
colnames(apr2022p_pre1)[12] <- "Loan_Purpose"
colnames(apr2022p_pre1)[13] <- "Property_Type_Final"

colnames(apr2022p_pre1)[16] <- "Endorsement_Year"
colnames(apr2022p_pre1)[17] <- "Endorsement_Month"


may2022p_pre <- read_xlsx("~/Downloads/FHA_SFSnapshot_May2022.xlsx", sheet = "Purchase Data May 2022")

may2022p_pre1 <- may2022p_pre %>% filter(`Property State` == "IL", `Property City` == "CHICAGO")

jun2022p_pre <- read_xlsx("~/Downloads/FHA_SFSnapshot_Jun2022.xlsx", sheet = "Purchase Data June 2022")

jun2022p_pre1 <- jun2022p_pre %>% filter(`Property State` == "IL", `Property City` == "CHICAGO")

jul2022p_pre <- read_xlsx("~/Downloads/CopyofFHASFSnapshotForward_July2022.xlsx", sheet = "Purchase Data July 2022")

jul2022p_pre1 <- jul2022p_pre %>% filter(`Property State` == "IL", `Property City` == "CHICAGO")

aug2022p_pre <- read_xlsx("~/Downloads/FHA SFSnapshot Forward_Aug2022.xlsx", sheet = "Purchase Data August 2022")

aug2022p_pre1 <- aug2022p_pre %>% filter(`Property State` == "IL", `Property City` == "CHICAGO")

sept2022p_pre <- read_xlsx("~/Downloads/FHA_SFSnapshot Forward_Sept_2022.xlsx", sheet = "Purchase Data September 2022")

sept2022p_pre1 <- sept2022p_pre %>% filter(`Property State` == "IL", `Property City` == "CHICAGO")

oct2022p_pre <- read_xlsx("~/Downloads/FHA_SFSnapshotForward_Oct_2022.xlsx", sheet = "Purchase Data October 2022")

oct2022p_pre1 <- oct2022p_pre %>% filter(`Property State` == "IL", `Property City` == "CHICAGO")

nov2022p_pre <- read_xlsx("~/Downloads/FHA_SFSnapshot Forward_Nov2022.xlsx", sheet = "Purchase Data November 2022")

nov2022p_pre1 <- nov2022p_pre %>% filter(`Property State` == "IL", `Property City` == "CHICAGO")

dec2022p_pre <- read_xlsx("~/Downloads/FHASFSnapshotForwardDec2022.xlsx", sheet = "Purchase Data December 2022")

dec2022p_pre1 <- jul2022p_pre %>% filter(`Property State` == "IL", `Property City` == "CHICAGO")


FHA_zip <- rbind(jan2023p_pre1, feb2023p_pre1, jan2022p_pre1, feb2022p_pre1, mar2022p_pre1, apr2022p_pre1, may2022p_pre1, jun2022p_pre1, jul2022p_pre1, aug2022p_pre1, sept2022p_pre1, oct2022p_pre1, nov2022p_pre1, dec2022p_pre1)

write.csv(FHA_zip, file = "FHA_zip.csv", na = "")
```

